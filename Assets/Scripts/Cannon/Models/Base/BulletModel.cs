﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Bullet", menuName = "VrCannon/Bullets/Bullet", order = 1)]
public class BulletModel : ScriptableObject
{
    //[SerializeField]
    //private GameObject prefab;
    //public GameObject Prefab { get { return prefab; } set => prefab = value; }

    [SerializeField]
    private float speed;
    public float Speed { get => speed; set => speed = value; }

    [SerializeField]
    GameObject bulletTemplate;
    public GameObject BulletTemplate { get => bulletTemplate; set => bulletTemplate = value; }

    [SerializeField]
    private GameObject shootEffectTemplate;
    public GameObject ShootEffectTemplate { get => shootEffectTemplate; set => shootEffectTemplate = value; }
}
