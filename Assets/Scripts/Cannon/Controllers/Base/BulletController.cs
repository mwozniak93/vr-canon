﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PoolObject))]
public class BulletController : MonoBehaviour, ICollidable
{
    private Transform cannon;

    //public BulletModel BulletModel;
    [SerializeField]
    private new Rigidbody rigidbody;
    private DistanceMeasurer distanceMeasurer = new DistanceMeasurer();
    [SerializeField]
    private LineDrawer lineDrawer;
    [SerializeField]
    private bool isGrounded = false;
    [SerializeField]
    private bool isTracked = false;
    [SerializeField]
    private GameData gameData;

    public void Initialize(Transform cannon)
    {
        isGrounded = false;
        this.cannon = cannon;
    }

    public void Move(Vector3 direction)
    {

        rigidbody.velocity = direction;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    public void HandleCollisionEnter(Collider collider)
    {

    }

    public void HandleCollisionExit(Collider collider)
    {
        isTracked = false;
        lineDrawer.DrawLine(Vector3.zero, Vector3.zero);
        gameData.MeasuredDistance.Set(0);

    }

    void Update()
    {
        if (rigidbody.velocity.magnitude > 0 && isTracked)
        {
            lineDrawer.DrawLine(transform.position, cannon.position); //In case it is moving we need to redraw the line
            UpdateDistance();
        }
    }

    public void HandleCollisionConfirm(Collider collider)
    {
        if (!isGrounded) return;
        isTracked = true;
        lineDrawer.DrawLine(transform.position, cannon.position);
        UpdateDistance();
    }

    private void UpdateDistance()
    {
        float distance = distanceMeasurer.GetDistance(transform.position, cannon.position);
        gameData.MeasuredDistance.Set(distance);
    }
}
