﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCannonController : MonoBehaviour
{

    [SerializeField]
    protected BaseShootComponent shootComponent;
    [SerializeField]
    protected BulletModel bulletModel;
    protected PoolManager poolManager;
    [SerializeField]
    private Transform cannonBarrel;
    private PoolObject shootEffectTemplate, bulletTemplate;

    protected virtual void Start()
    {
   
        poolManager = ManagersContainer.Instance.GetManager<PoolManager>();
        InitializeEffects();
    }

    protected virtual void InitializeEffects()
    {

        poolManager.Init(bulletModel.BulletTemplate, bulletModel.ShootEffectTemplate);
        shootEffectTemplate = bulletModel.ShootEffectTemplate.GetComponent<PoolObject>();
        bulletTemplate = bulletModel.BulletTemplate.GetComponent<PoolObject>();

    }

    public virtual void ShootBullet()
    {
       

        GameObject shootEffect = poolManager.Spawn(shootEffectTemplate.PoolKey, cannonBarrel.position);
        GameObject bullet = poolManager.Spawn(bulletTemplate.PoolKey, cannonBarrel.position);
        BulletController bulletController = bullet.GetComponent<BulletController>();
        bulletController.Initialize(transform);
        shootComponent.Shoot(bulletController, bulletModel.Speed);
    }
}
