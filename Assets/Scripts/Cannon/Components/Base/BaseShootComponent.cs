﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseShootComponent : MonoBehaviour
{

  

    public abstract void Shoot(BulletController bullet, float speed);

}
