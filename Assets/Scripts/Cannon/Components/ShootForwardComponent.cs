﻿using UnityEngine;
using System.Collections;

public class ShootForwardComponent : BaseShootComponent
{

    // Use this for initialization
    void Start()
    {

    }


    public override void Shoot(BulletController bullet, float speed)
    {
        Vector3 direction = transform.forward * speed;
        bullet.Move(direction);


    }
}
