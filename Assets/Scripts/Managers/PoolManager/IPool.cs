﻿using UnityEngine;
//using ArShooter.Managers.Helpers;
using System.Collections.Generic;


	public interface IPool
	{
	GameObject Spawn (string key, Vector3 pos, bool shouldBeActive);

		GameObject Despawn (string key, string name, bool detach = false);

	void Init (params GameObject[] objects);
	}
