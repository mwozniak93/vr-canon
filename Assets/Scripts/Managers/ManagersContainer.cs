﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagersContainer : MonoBehaviour
{

    public static ManagersContainer Instance;
    [SerializeField]
    private Dictionary<string, Component> managers = new Dictionary<string, Component>();

    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public T GetManager<T>() where T : Component
    {
        T manager;
        string key = (typeof(T)).ToString();
        if (managers.ContainsKey(key))
        {
            manager = managers[key] as T;
        }
        else
        {
            Debug.Log("Adding manager : " + key);
            manager = GetComponent<T>();
            managers.Add((typeof(T)).ToString(), manager);
        }
        if (manager == null)
        {
            Debug.LogError("Manager does not exist : " + typeof(T).ToString());
        }
        return manager;
    }

}
