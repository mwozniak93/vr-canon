﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "VrCannon/Create global Game Data container", order = 2)]
public class GameData : ScriptableObject
{
    [SerializeField]
    private FloatValue measuredDistance;
    public FloatValue MeasuredDistance { get => measuredDistance; set => measuredDistance = value; }

    [SerializeField]
    private FloatValue currentGazeTime;
    public FloatValue CurrentGazeTime { get => currentGazeTime; set => currentGazeTime = value; }

    [SerializeField]
    private float maxGazeTime;
    public float MaxGazeTime { get => maxGazeTime; set => maxGazeTime = value; }


}
