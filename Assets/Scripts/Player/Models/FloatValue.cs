﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "FloatValue", menuName = "VrCannon/Create Float Value", order = 2)]
public class FloatValue : ScriptableObject
{
    [SerializeField]
    private float value;

    public float Value
    {
        get { return value; }
        private set
        {
            this.value = (float)Math.Round((decimal)value, 2);
            OnChange.Invoke(this.value);
        }
    }

    public event Action<float> OnChange = delegate { };

    public void Set(float newValue)
    {
        Value = newValue;
    }
    public void IncreaseByOne()
    {
        Value += 1;
    }
}
