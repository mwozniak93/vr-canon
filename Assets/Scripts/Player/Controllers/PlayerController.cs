﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private MushroomCharacterController characterController;
    [SerializeField]
    GazeController gazeController;
    private ICollidable colisionHandler;
    // Use this for initialization
    void Awake()
    {
        gazeController.OnHitEnter += GazeController_OnHitConfirmed;
        gazeController.OnHitExit += GazeController_OnHitExit;
        gazeController.OnHitConfirm += GazeController_OnHitConfirm;
    }

    private void GazeController_OnHitConfirm(Collider collider)
    {
        if (colisionHandler != null) colisionHandler.HandleCollisionConfirm(collider);
    }

    private void GazeController_OnHitExit(Collider collider)
    {
        //Debug.Log("GazeController_OnHitExit");
        if (colisionHandler != null) colisionHandler.HandleCollisionExit(collider);
        colisionHandler = null;
    }

    private void GazeController_OnHitConfirmed(Collider collider)
    {
        //Debug.Log("GazeController_OnHitConfirmed");
        colisionHandler = collider.GetComponent<ICollidable>();
        if (colisionHandler != null)
        {
            colisionHandler.HandleCollisionEnter(collider);

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
