﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeController : MonoBehaviour
{
    
    Camera camera;
    [SerializeField]
    float gazeTime = 0;
    bool isColliderDetected = false;
    private Collider curentCollider;
    public event Action<Collider> OnHitEnter = delegate { };
    public event Action<Collider> OnHitExit = delegate { };
    public event Action<Collider> OnHitConfirm = delegate { };
    [SerializeField]
    private GameData gameData;

    void Start()
    {
        camera = Camera.main;
    }


    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        int layerMask = LayerMask.GetMask("Interactable");
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, 100, layerMask))
        {
            if (hit.collider == null) return;
            if (curentCollider != null)
            {
                if (curentCollider.GetInstanceID() != hit.collider.GetInstanceID())
                {
                    OnHitExit.Invoke(hit.collider);
                    gazeTime = 0;
                    gameData.CurrentGazeTime.Set(gazeTime);
                }
            }
            curentCollider = hit.collider;
            isColliderDetected = true;
            OnHitEnter.Invoke(hit.collider);
            gazeTime += Time.deltaTime;
            gameData.CurrentGazeTime.Set(gazeTime);
            if (gazeTime >= gameData.MaxGazeTime)
            {
                //Debug.Log("gazeTime : " + gazeTime + " confirmationDelay : " + confirmationDelay);
                OnHitConfirm.Invoke(hit.collider);
                gazeTime = 0;
                gameData.CurrentGazeTime.Set(gazeTime);
            }

            //Debug.Log("Hit : " + hit.collider.name);
        }
        else
        {
            gazeTime = 0;
            gameData.CurrentGazeTime.Set(gazeTime);
            if (isColliderDetected)
            {
                OnHitExit.Invoke(hit.collider);
            }
            isColliderDetected = false;

        }
    }
}
