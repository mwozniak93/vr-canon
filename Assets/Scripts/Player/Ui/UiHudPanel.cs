﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UiHudPanel : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI distanceLabel;
    [SerializeField]
    private CanvasGroup canvasGroup;
    private bool isShown;
    [SerializeField]
    private GameData gameData;
    [SerializeField]
    Image timeBar;
    // Start is called before the first frame update
    void Start()
    {
        gameData.MeasuredDistance.OnChange += MeasuredDistance_OnChange;
        gameData.CurrentGazeTime.OnChange += CurrentGazeTime_OnChange;
    }

    private void CurrentGazeTime_OnChange(float value)
    {
        timeBar.fillAmount = value / gameData.MaxGazeTime;
        if (value >= gameData.MaxGazeTime && timeBar.gameObject.activeInHierarchy)
        {
            timeBar.gameObject.SetActive(false);
        }
        else if (value < gameData.MaxGazeTime && !timeBar.gameObject.activeInHierarchy)
        {
            timeBar.gameObject.SetActive(true);
        }
    }

    private void MeasuredDistance_OnChange(float distance)
    {

        distanceLabel.text = string.Format("Distance : {0} ", distance);
        if (isShown) return;
        ShowPanel();
    }

    void ShowPanel()
    {
        isShown = true;
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
        canvasGroup.interactable = true;
    }

    void HidePanel()
    {
        isShown = false;
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.interactable = false;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
