﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollidable
{
    void HandleCollisionEnter(Collider collider);
    void HandleCollisionExit(Collider collider);
    void HandleCollisionConfirm(Collider collider);
}
