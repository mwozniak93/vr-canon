﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivator : MonoBehaviour
{
    [SerializeField]
    private float time;
    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(Deactivate());
    }
    private IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(time);
    }
    
}
