﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceMeasurer
{
    public float GetDistance(Vector3 pointA, Vector3 pointB)
    {
        float distance = Mathf.Abs(Vector3.Distance(pointA, pointB));
        return distance;
    }
}
