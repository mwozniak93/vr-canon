﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
    [SerializeField]
    private LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void DrawLine(Vector3 pointA, Vector3 pointB)
    {
        lineRenderer.SetPosition(0, pointA);
        lineRenderer.SetPosition(1, pointB);
        Debug.Log("Draawing line");
    }
}
