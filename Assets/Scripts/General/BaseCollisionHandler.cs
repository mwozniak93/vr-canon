﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCollisionHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public abstract void PerformAction();


}
