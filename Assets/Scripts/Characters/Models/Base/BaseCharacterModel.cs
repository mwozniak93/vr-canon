﻿using UnityEngine;

public abstract class BaseCharacterModel : ScriptableObject
{

    [SerializeField]
    private new string name;
    public string Name { get => name; set => name = value; }

    [SerializeField]
    private GameObject chargeEffectTemplate;
    public GameObject ChargeEffectTemplate { get => chargeEffectTemplate; set => chargeEffectTemplate = value; }

    //[SerializeField]
    //private float timeToShoot;
    //public float TimeToShoot { get => timeToShoot; set => timeToShoot = value; }


}
