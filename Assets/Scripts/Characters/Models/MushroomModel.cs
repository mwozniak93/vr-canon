﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Mushroom Character", menuName = "VrCannon/Characters/Mushroom", order = 1)]
public class MushroomModel : BaseCharacterModel
{

    [SerializeField]
    private Color color;
    public Color Color { get => color; set => color = value; }




    //[SerializeField]
    //private Color color;
    //public Color Color { get { return color; } set => color = value; }

}
