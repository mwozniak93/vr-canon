﻿using UnityEngine;
using System.Collections;

public class MushroomCharacterController : BaseCharacterController<MushroomModel>
{
    [SerializeField]
    private Animator animator;

    public override void StopCannonCharge()
    {
        animator.SetBool("Charging", false);
        base.StopCannonCharge();
    }
    public override void ChargeCannon()
    {
        animator.SetBool("Charging", true);
        base.ChargeCannon();
    }

    public override void FinishCharging()
    {
        //if (chargingTime >= Character.TimeToShoot)
        //{
            animator.SetTrigger("Attack");
        //    chargingTime = 0;
        //}
    }
    public override void ShootFromCannon()
    {
        Debug.Log("ShootFromCannon");
        base.ShootFromCannon();
    }
}
