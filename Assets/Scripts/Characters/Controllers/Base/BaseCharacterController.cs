﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCharacterController<T> : MonoBehaviour, ICollidable where T : BaseCharacterModel
{
    public T Character;
    private PoolManager poolManager;

    [SerializeField]
    bool isCharging = false;
    GameObject chargeEffect = null;
    [SerializeField]
    protected BaseCannonController cannonController;
    [SerializeField]
    private Transform center;

    protected void Start()
    {
        poolManager = ManagersContainer.Instance.GetManager<PoolManager>();
        InitializeChargeEffect();
    }
    private void InitializeChargeEffect()
    {
        poolManager.Init(Character.ChargeEffectTemplate);
    }
    public virtual void ChargeCannon()
    {

        if (chargeEffect == null)
        {
            chargeEffect = poolManager.Spawn(Character.ChargeEffectTemplate.GetComponent<PoolObject>().PoolKey, center.position);
        }

        //FinishCharging();
    }

    public virtual void FinishCharging()
    {
        ShootFromCannon();

    }
    public virtual void ShootFromCannon()
    {
        cannonController.ShootBullet();
    }




    public virtual void StopCannonCharge()
    {
        if (chargeEffect.activeInHierarchy)
        {
            chargeEffect.SetActive(false);
            chargeEffect = null;
            isCharging = false;
        }
    }



    public virtual void HandleCollisionEnter(Collider collider)
    {
        isCharging = true;
        ChargeCannon();
    }

    public void HandleCollisionExit(Collider collider)
    {
        isCharging = false;
        StopCannonCharge();
    }

    public void HandleCollisionConfirm(Collider collider)
    {
        FinishCharging();
    }
}
